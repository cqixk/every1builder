/* global BootstrapDialog */

/**
 * Show alert message
 * 
 * @param   {String}    message     Message
 * @param   {String}    title       Optional title
 */
function bootstrap_alert(message, title) {
    if (window.console && console.log) {
        console.log('bootstrap_alert', message);
    }
    BootstrapDialog.alert({
        title: title || 'Error',
        message: message,
        type: BootstrapDialog.TYPE_DANGER,
        closable: true,
        draggable: true
    });
}

$('.app-open-dir').on('click', function () {
    var $btn = $(this).button('opening');

    $.get('/prepare/open/' + $btn.data('path-name'))
        .fail(function (xhr) {
            bootstrap_alert(xhr.responseText);
        })
        .always(function () {
            $btn.button('reset');
        });
});

$('.app-run-app').on('click', function () {
    var $btn = $(this).button('opening');

    $.get('/build/run/' + $btn.data('path-name'))
        .fail(function (xhr) {
            bootstrap_alert(xhr.responseText);
        })
        .always(function () {
            $btn.button('reset');
        });
});