/* global __dirname, module */

"use strict";

var express = require('express');
var expressValidator = require('express-validator')
var helmet = require('helmet');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var e1main = require('./lib/e1main');

var rIndex = require('./routes/index');
var rCreate = require('./routes/create');
var rModify = require('./routes/modify');
var rPrepare = require('./routes/prepare');
var rBuild = require('./routes/build');
var rList = require('./routes/list');

var app = express();

// Security
app.use(helmet());

// Favicon
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

// View engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// An auto archived log stream
var accessLogStream = require("stream-file-archive")({
    path: "logs/app-%Y-%m-%d.log", // Write logs rotated by the day
    symlink: "logs/current.log", // Maintain a symlink called current.log
    compress: true, // Gzip old log files
});

app.use(logger('combined', {stream: accessLogStream}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(expressValidator({
    customSanitizers: {
        replaceUmlauts: e1main.replaceUmlauts,
    }
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/libs', express.static(path.join(__dirname, 'bower_components')));
app.use('/userapps', express.static(path.join(__dirname, 'userapps')));

// Routing
app.use('/', rIndex);
app.use('/create', rCreate);
app.use('/modify', rModify);
app.use('/prepare', rPrepare);
app.use('/build', rBuild);
app.use('/list', rList);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
