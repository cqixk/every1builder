/* global exports */

"use strict";

var fs = require('fs'),
    async = require('async'),
    path = require('path'),
    appRoot = require('app-root-path').toString();
var debug = require('debug')('app:data');

/**
 * Get display value for value
 * 
 * @param {type} folderPath
 * @param {type} filename
 * @param {type} callback
 * @returns {Promise}
 */
exports.load = function(folderPath, filename, callback) {
    async.waterfall([
        function(callback) {
            //Get valid script path
            var dataPathJs = path.join(folderPath, filename+'.js');
            var dataPathLs = path.join(folderPath, filename+'.ls');
            try {
                debug('Start stat');
                async.map([folderPath, dataPathJs, dataPathLs], 
                    function(dirPath, callback) {
                        fs.stat(dirPath, function(err, stat) {
                            if (err) {
                                if (err.code==='ENOENT') {
                                    callback(false, false);
                                } else {
                                    callback(err);
                                }
                            } else {
                                callback(false, stat);
                            }
                        });
                    }, function(err, stats){
                        if (err) {
                            callback(err);
                        } else if (!stats[0] || !stats[0].isDirectory()) {
                            callback('Data directory missing');
                        } else if (stats[1] && stats[1].isFile()) {
                            callback(false, dataPathJs, 'javascript');
                        } else if (stats[2] && stats[2].isFile()) {
                            callback(false, dataPathLs, 'livescript');
                        } else {
                            var error = new Error('Neither '+path.basename(dataPathJs)+' nor '+path.basename(dataPathLs)+' exists within '+folderPath);
                            error.allFilesMissing = true;
                            callback(error);
                        }
                    }
                );
            } catch (err) {
                callback('Error in VM - init paths: '+err);
            }
        },
        function(dataPathFile, language, callback) {
            //Load script file
            debug('Read file', dataPathFile);
            fs.readFile(dataPathFile, 'utf8', function(err, script) {
                callback(
                    err, 
                    {   
                        folderPath: folderPath,
                        file: dataPathFile, 
                        script: script, 
                        language: language
                    }
                );
            });
        },
        function(loadedFile, callback) {
            if (loadedFile.language==='livescript') {
                loadExecuteLiveScript(loadedFile, callback);
            } else {
                callback(false, loadedFile);
            }
        }
    ], callback);
};

/**
 * Load execute includes for livescript
 * 
 * @param {type} loadedFile
 * @param {type} callback
 * @returns {undefined}
 */
function loadExecuteLiveScript(loadedFile, callback) {
    //Split for parts (execute...)
    var partsRegex = /^([ \t]*execute)[ \t]*["']?([^\t\\"']+)["']?;?$/mi; 
    var sources = loadedFile.script.split(partsRegex);
    //debug('sources', sources);

    //Load included sources
    var mergedSource = '',
        lastExecute=false,
        lastIndent='',
        executeToLoad={};

    //Find files to load
    sources.forEach(function(element) {
        if (element.toLowerCase().endsWith('execute')) {
            lastExecute = true;
        } else if (lastExecute) {
            lastExecute = false;

            executeToLoad[element] = element;
        }
    });
    
    //Merge all files
    async.map(executeToLoad, function (fileName, callback) {
        //Load file
        var dataPathFile = path.join(loadedFile.folderPath, fileName.replace(/ /g, '_')+'.ls');
        debug('Read sub file', dataPathFile);
        fs.readFile(dataPathFile, {encoding: 'utf8'}, callback);
    }, function (err, results) {
        if (err) {
            callback(err); return;
        }

        //Process again
        sources.forEach(function(element) {
            if (element.toLowerCase().endsWith('execute')) {
                lastExecute = true;

                //Save indent, so we can add the indent to the included source
                lastIndent = element.toLowerCase().replace('execute', '');
            } else if (lastExecute) {
                lastExecute = false;

                //Indent included source and add to merged source
                mergedSource += results[element].replace(/.+/g, lastIndent+'$&');
            } else {
                //Add source to merged source
                mergedSource += element;
            }
        });
        
        //debug('mergedSource', mergedSource);
        loadedFile.script = mergedSource;
        callback(false, loadedFile);
    });
    
}

