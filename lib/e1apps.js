/* global exports, err, require */

var e1consts = require('./e1consts'),
    e1main = require('./e1main'),
    appRoot = require('app-root-path').toString(),
    fs = require('fs.extra'),
    path = require('path'),
    async = require('async'),
    underscore = require('underscore'),
    debug = require('debug')('Every1Builder:server');

/**
 * Create new app
 * 
 * @param   {String}        name        Name
 * @param   {Integer}       port        Port
 * @param   {function}      callback    Callback
 * @returns {String}
 */
exports.create = function(name, port, callback) {
    var _all_args = 'create['+underscore.initial(arguments).join(' | ')+']';
    var pathName = e1main.removeNonAlphaNumericSlash(e1main.camelize(e1main.replaceUmlauts(name)));
    
    async.waterfall([
        function(callback) {
            //Create folder, on exists exit
            fs.mkdir(path.join(appRoot, e1consts.USERAPPS, pathName), function (err) {
                debug('Created path %s', pathName, err);
                callback(err);
            });
        },
        function(callback) {
            //Create images folder, on exists exit
            fs.mkdir(path.join(appRoot, e1consts.USERAPPS, pathName, 'images'), function (err) {
                debug('Created path %s', pathName+' - '+'images', err);
                callback(err);
            });
        },
        function(callback) {
            //Add info database
            var nosql = e1main.openInfo(pathName);

            nosql.insert({
                type: e1consts.BASE,
                name: e1main.removeNonAlphaNumericUmlauts(name),
                port: port,
                randkey: require('crypto').randomBytes(48).toString('hex')
            },function (err, count) {
                debug('Database created. Rows:%i', count, err);
                callback(err);
            }, 'Start app info');
        },
        function(callback) {
            //Add xlsx menu files
            fs.copy(
                path.join(appRoot, e1consts.PRESETS, e1consts.MENUS_FILE), 
                path.join(appRoot, e1consts.USERAPPS, pathName, e1consts.MENUS_FILE), {replace: false}, 
                function (err) {
                    debug('Copied %s', e1consts.MENUS_FILE, err);
                    callback(err);
                }
            );
        },
        function(callback) {
            //Add xlsx form files
            fs.copy(
                path.join(appRoot, e1consts.PRESETS, e1consts.FORMS_FILE), 
                path.join(appRoot, e1consts.USERAPPS, pathName, e1consts.FORMS_FILE), {replace: false}, 
                function (err) {
                    debug('Copied %s', e1consts.MENUS_FILE, err);
                    callback(err);
                }
            );
        }
    ], function (err) {
        if (err) {
            console.error(_all_args+' error %s', err);
        }
        callback(err, pathName);
    });
};

/**
 * Modify app
 * 
 * @param   {String}    pathName    Path name
 * @param   {String}    name        Name
 * @param   {Integer}   port        Port
 * @param   {String}    numlocal
 * @param   {String}    datelocal
 * @param   {String}    dbtype
 * @param   {String}    dbdatabase
 * @param   {String}    dbhost
 * @param   {Integer}   dbport
 * @param   {String}    dbusername
 * @param   {String}    dbpassword
 * @returns {object}
 */
exports.modify = function(pathName, name, port, numlocal, datelocal, dbtype, dbdatabase, dbhost, dbport, dbusername, dbpassword) {
    //Add info database
    var nosql = e1main.openInfo(pathName);
    
    nosql.update(function(doc) {
        if (doc.type===e1consts.BASE) {
            doc.name = e1main.removeNonAlphaNumericUmlauts(name);
            doc.port = port;
            doc.numlocal = numlocal;
            doc.datelocal = datelocal;
            doc.dbtype = dbtype;
            doc.dbdatabase = dbdatabase;
            doc.dbhost = dbhost;
            doc.dbport = dbport;
            doc.dbusername = dbusername;
            doc.dbpassword = dbpassword;
            doc.randkey = require('crypto').randomBytes(48).toString('hex');
        }
        return doc;
    }, 'Modify app info');
}

/**
 * Get apps list
 * 
 * @param   {function}      callback    Callback
 */
exports.list = function(callback) {
    var srcpath = path.join(appRoot, e1consts.USERAPPS);
    var paths = fs.readdirSync(srcpath).filter(function(file) {
        return fs.statSync(path.join(srcpath, file)).isDirectory();
    });
    
    async.map(paths, e1main.openInfo_GetBase, callback);
};

/**
 * Build app
 * 
 * @param   {String}        pathName    Path name
 * @param   {function}      callback    Callback
 */
exports.build = function(pathName, callback) {
    var _all_args = 'build['+underscore.initial(arguments).join(' | ')+']';
    var steps = ['Init'];
    var e1builder = require('./e1builder');
    
    async.waterfall([
        async.apply(getBaseInfo, pathName, steps),
        readXlsxConfigs,
        function(app, configs, callback) {
            //Create app
            e1builder.build(
                app,
                configs.forms,
                configs.menus,
                function(err, build) {
                    if (err) {
                        callback(err); return;
                    }
                    steps.push('Build done');
                    debug('build done', build);

                    app.forms = configs.forms;
                    app.menus = configs.menus;
                    callback(false, app);
                }
            );
        }
    ], function (err, app) {
        if (err) {
            console.error(_all_args+' error %s', err);
        }
        callback(err, app, steps);
    });
};

/**
 * Prepare app
 * 
 * @param   {String}        pathName    Path name
 * @param   {function}      callback    Callback
 */
exports.prepare = function(pathName, callback) {
    var _all_args = 'prepare['+underscore.initial(arguments).join(' | ')+']';
    var steps = ['Init'];
    var e1prepare = require('./e1prepare');
    
    async.waterfall([
        async.apply(getBaseInfo, pathName, steps),
        readXlsxConfigs,
        function(app, configs, callback) {
            //Create code stubs
            async.parallel({
                forms: function(asyncCallbackParallel) {
                    //Prepare client script files for forms
                    async.map(
                        configs.forms,
                        function(config_page, asyncCallbackMap) {
                            e1prepare.prepare_script(config_page.name, app.pathName, e1consts.FORMS_PREFIX, e1consts.FORMS_SCRIPT_PARTS.client, asyncCallbackMap)
                        },
                        asyncCallbackParallel
                    );
                },
                forms_models: function(asyncCallbackParallel) {
                    //Prepare model script files for forms
                    async.map(
                        configs.forms,
                        function(config_page, asyncCallbackMap) {
                            e1prepare.prepare_script(config_page.name, app.pathName, e1consts.FORMS_MODEL_PREFIX, e1consts.FORMS_SCRIPT_PARTS.model, asyncCallbackMap)
                        },
                        asyncCallbackParallel
                    );
                },
                menus: function(asyncCallbackParallel) {
                    //Prepare client script files for menus
                    async.map(
                        configs.menus,
                        function(config_page, asyncCallbackMap) {
                            e1prepare.prepare_script(config_page.name, app.pathName, e1consts.MENUS_PREFIX, e1consts.MENUS_SCRIPT_PARTS.client, asyncCallbackMap)
                        },
                        asyncCallbackParallel
                    );
                },
                menus_routes: function(asyncCallbackParallel) {
                    //Prepare page script files for menus
                    var menu = underscore.findWhere(configs.menus, {
                        name: 'Main'
                    });
                    if (menu===null) {
                        return asyncCallbackParallel('Cannot create app without "Main" menu.');
                    }

                    async.map(
                        underscore.filter(menu.data, function(field) {
                            switch (field.Action) {
                                case 'Open own':
                                    return true;
                                case 'Open form': //@todo Maybe add route script file - But no route script file for now
                                case 'Logout':
                                    return false;
                                default:
                                    return asyncCallbackParallel(new Error('prepare menus_routes - Unknown menu type: '+field.Action));
                            }
                        }),
                        function(config_page, asyncCallbackMap) {
                            debug('prepare menus_routes: ', config_page);
                            e1prepare.prepare_script(config_page['Item Name'], app.pathName, e1consts.MENUS_PREFIX_ROUTE, e1consts.MENUS_SCRIPT_PARTS.routes, asyncCallbackMap)
                        },
                        asyncCallbackParallel
                    );
                }
            },
            function(err, prepared) {
                if (err) {
                    callback(err); return;
                }
                steps.push('Prepare done');
                
                console.error('prepared', prepared);

                app.forms = prepared.forms;
                app.forms_models = prepared.forms_models;
                app.menus = prepared.menus;
                app.menus_routes = prepared.menus_routes;
                callback(false, app);
            });
        }
    ], function (err, app) {
        if (err) {
            console.error(_all_args+' error %s', err);
        }
        callback(err, app, steps);
    });
};

/**
 * Get base info
 * 
 * @param {type} pathName
 * @param {type} steps
 * @param {type} callback
 */
function getBaseInfo(pathName, steps, callback) {
    e1main.openInfo_GetBase(pathName, function(err, app) {
        if (err) {
            callback(err); return;
        }
        steps.push('Got app info');
        callback(false, app, steps);
    });
}

/**
 * Read configs from XLSX
 * 
 * @param {type} app
 * @param {type} steps
 * @param {type} callback
 */
function readXlsxConfigs(app, steps, callback) {
    var _all_args = 'readXlsxConfigs['+underscore.initial(arguments).join(' | ')+']';
    var e1xlsx = require('./e1xlsx');
    
    async.parallel({
        forms: function(asyncCallback) {
            e1xlsx.read_configs(path.join(appRoot, e1consts.USERAPPS, app.pathName, e1consts.FORMS_FILE), asyncCallback);
        },
        menus: function(asyncCallback){
            e1xlsx.read_configs(path.join(appRoot, e1consts.USERAPPS, app.pathName, e1consts.MENUS_FILE), asyncCallback);
        }
    },
    function(err, configs) {
        if (err) {
            callback(err);
            return console.error(_all_args+' error %s', err);
        }
        steps.push('Got xlsx info');
        callback(false, app, configs);
    });
}