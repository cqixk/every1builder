/* global exports */

var xlsx = require('xlsx'),
    debug = require('debug')('Every1Builder:server');

/**
 * Read configs from XLSX file
 * 
 * @param   {String}        path        XLSX file path
 * @param   {function}      callback    Callback
 */
exports.read_configs = function(path, callback) {
    var workbook = xlsx.readFile(path);
    
    var length = workbook.SheetNames.length;
    var reply = [length];
    for (var index = 0; index < length; ++index) {
        var sheet_name = workbook.SheetNames[index];
        reply[index] = {
            'data': xlsx.utils.sheet_to_json(workbook.Sheets[sheet_name], {range: 'A1:C101'}),
            'name': sheet_name,
            'index': index
        };
        debug('read_configs '+path, reply);
    }
    
    callback(false, reply);
};