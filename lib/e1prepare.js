/* global require, exports */

var e1consts = require('./e1consts'),
    e1main = require('./e1main'),
    appRoot = require('app-root-path').toString(),
    fs = require('fs.extra'),
    path = require('path'),
    underscore = require('underscore'),
    Type = require('type-of-is'),
    debug = require('debug')('Every1Builder:server');

/**
 * Prepare script
 * 
 * @param   {String}        itemName        Item Name
 * @param   {String}        pathName        App path name
 * @param   {String}        prefix          File name prefix
 * @param   {Array}         scriptParts     Script parts
 * @param   {function}      asyncCallback   Callback for async
 */
exports.prepare_script = function(itemName, pathName, prefix, scriptParts, asyncCallback) {
    var NL = "\n";
    var _all_args = 'prepare_script['+underscore.initial(arguments).join(' | ')+']';
    var reply = {
        name: itemName,
        pathName: prefix+e1main.sanitiseLink(itemName)+e1consts.SCRIPT_ENDING
    };
    var fullPath = path.join(appRoot, e1consts.USERAPPS, pathName, reply.pathName);
    
    fs.access(fullPath, fs.F_OK, function (err) {
        if (err) {
            //File doesn't exist
            debug('Process '+reply.pathName, reply);
            
            //Prepare file content
            var sep = NL+NL+'on ';
            var content = '# '+prefix.replace(/_/g, ' ')+'actions for '+reply.name;
            
            var scriptPart;
            for (var i = 0, leni = scriptParts.length; i < leni; i++) {
                scriptPart = scriptParts[i];
                
                if (Type.is(scriptPart, Array)) {
                    content += sep+scriptPart[0]+NL+scriptPart[1];
                } else {
                    content += sep+scriptPart;
                }
            }
            
            fs.writeFile(fullPath, content, function(err) {
                if (err) {
                    asyncCallback(err, false);
                    return console.error(_all_args+' error %s', err);
                }
                asyncCallback(false, reply);
            });
        } else {
            // File exist
            asyncCallback(false, reply);
        }
    });
};