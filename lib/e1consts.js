/**
 * Global constants
 * 
 * @private
 */
const USERAPPS = 'userapps';
const BASE = 'base';
const PRESETS = 'presets';
const FORMS_FILE = 'forms.xlsx';
const MENUS_FILE = 'menus.xlsx';
const FORMS_PREFIX = 'forms_';
const FORMS_MODEL_PREFIX = 'forms_model_';
const MENUS_PREFIX = 'menus_';
const MENUS_PREFIX_ROUTE = 'menus_routes_';
const SCRIPT_ENDING = '.ls';
const FORMS_SCRIPT_PARTS = {
    client: [
        'load',
        'save'
    ],
    model: [
        'before', //beforeValidate
        'beforecreate', //beforeCreate
        'beforedestroy', //beforeDestroy
        'beforeupdate', //beforeUpdate
        'aftercreate', //afterCreate
        'afterdestroy', //afterDestroy
        'afterupdate', //afterUpdate
        'fieldslist', //Router on fieldslist
        'findall', //Router on findAll get
    ]
};
const MENUS_SCRIPT_PARTS = {
    client: [
        'load'
    ], 
    routes: [
        ['get_/', 'res.render "empty" {info: "Hallo"}'],
        ['get_/data', 'res.json "Hallo"'],
        ['post_/data', 'res.json "Hallo"'],
        ['put_/data/:id', 'res.json "Hallo"'],
        ['delete_/data/:id', 'res.json "Hallo"']
    ]
};
const COMPILE_MODE_CLIENT = 1;
const COMPILE_MODE_MODULE = 2;
const COMPILE_MODE_ROUTE = 3;

module.exports = {
    USERAPPS: USERAPPS,
    BASE: BASE,
    PRESETS: PRESETS,
    FORMS_FILE: FORMS_FILE,
    MENUS_FILE: MENUS_FILE,
    FORMS_PREFIX: FORMS_PREFIX,
    FORMS_MODEL_PREFIX: FORMS_MODEL_PREFIX,
    MENUS_PREFIX: MENUS_PREFIX,
    MENUS_PREFIX_ROUTE: MENUS_PREFIX_ROUTE,
    SCRIPT_ENDING: SCRIPT_ENDING,
    FORMS_SCRIPT_PARTS: FORMS_SCRIPT_PARTS,
    MENUS_SCRIPT_PARTS: MENUS_SCRIPT_PARTS,
    COMPILE_MODE_CLIENT: COMPILE_MODE_CLIENT,
    COMPILE_MODE_MODULE: COMPILE_MODE_MODULE,
    COMPILE_MODE_ROUTE: COMPILE_MODE_ROUTE
};