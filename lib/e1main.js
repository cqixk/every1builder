/* global require, exports */

var e1consts = require('./e1consts'),
    path = require('path'),
    appRoot = require('app-root-path').toString(),
    debug = require('debug')('Every1Builder:server'),
    docdefaults = require(__dirname + '/../config/appdefaults.json');

/**
 * Camelize string
 * 
 * @param   {String}    str         String
 * @returns {String}
 */
exports.camelize = function(str) {
    return str.replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, function (match, index) {
        if (+match === 0) return ""; // or if (/\s+/.test(match)) for white spaces
        return match.toUpperCase();
    });
};

/**
 * Remove non alpha numeric
 * 
 * @param   {String}    str         String
 * @returns {String}
 */
exports.removeNonAlphaNumeric = function(value) {
    return value.replace(/[^a-zA-Z0-9_]+/g, '');
};

/**
 * Remove non alpha numeric slash
 * 
 * @param   {String}    str         String
 * @returns {String}
 */
exports.removeNonAlphaNumericSlash = function(value) {
    return value.replace(/[^a-zA-Z0-9]+/g, '');
};

/**
 * Remove non alpha numeric and umlauts
 * 
 * @param   {String}    str         String
 * @returns {String}
 */
exports.removeNonAlphaNumericUmlauts = function(value) {
    return value.replace(/[^a-zA-ZäöüÄÖÜß0-9_ ]+/g, '');
};

/**
 * Replace umlauts
 * 
 * @param   {String}    str         String
 * @returns {String}
 */
exports.replaceUmlauts = function(value) {
    var tr = {'ä':'ae', 'ü':'ue', 'ö':'oe', 'ß':'ss', 'Ä':'Ae', 'Ü':'Ue', 'Ö':'Oe', ' ':'_'}
    return value.replace(/[äöüßÄÖÜ ]/g, function($0) { return tr[$0] });
};

/**
 * Sanitise as valid link
 * 
 * @param   {String}    str         String
 * @returns {String}
 */
exports.sanitiseLink = function(value) {
    return exports.removeNonAlphaNumeric(exports.replaceUmlauts(value)).toLowerCase();
}
        
/**
 * Open info database
 * 
 * @param   {String}        pathName    Path name
 * @returns {openInfo.nosql}
 */
exports.openInfo = function(pathName) {
    var nosql = require('nosql').load(path.join(appRoot, e1consts.USERAPPS, pathName, 'info.nosql'));
    nosql.on('error', function(err, source) {
        console.error('NOSQL ERROR: ' + err + ' Running: ' + source);
        throw new Error(err);
    });
    nosql.on('insert', function(begin, count) {
        console.error('NOSQL INSERTED: ' + begin + ' - ' + count);
    });
    return nosql;
};

/**
 * Get base of openInfo
 * 
 * @param   {String}        pathName    Path name
 * @param   {function}      callback    Callback
 */
exports.openInfo_GetBase = function(pathName, callback) {
    var nosql = exports.openInfo(pathName);
    
    nosql.one(
        function(doc) {
            if (doc.type===e1consts.BASE) {
                return doc;
            }
        }, 
        function(err, doc) {
            if (err) {
                console.error('Database error: %s', err);
                doc = null;
            } else if (doc===null) {
                err = 'Failed to get database values for '+pathName;
                console.error('Database logic error: %s', err);
            }
            debug('openInfo_GetBase', doc);
            
            //Clone docdefaults, because it would be overwritten else
            var docdefcopy = Object.assign({}, docdefaults);
            
            callback(err, {
                pathName: pathName,
                doc: Object.assign(docdefcopy, doc),
                folderPath: path.join(appRoot, e1consts.USERAPPS, pathName)
            });
        }
    );
};