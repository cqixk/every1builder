/* global require, exports */

var e1consts = require('./e1consts'),
    e1main = require('./e1main'),
    appRoot = require('app-root-path').toString(),
    ncp = require('ncp'),
    path = require('path'),
    async = require('async'),
    jsonFile = require('json-file-plus'),
    fs = require('fs.extra'),
    hogan = require('hogan.js'),
    underscore = require('underscore'),
    debug = require('debug')('Every1Builder:server');

/**
 * 
 * @param   {object}        app         App info
 * @param   {object}        forms       Read forms configs from XLSX
 * @param   {object}        menus       Read menus configs from XLSX
 * @param   {function}      callback    Callback
 */
exports.build = function(app, forms, menus, callback) {
    var _all_args = 'build['+underscore.initial(arguments).join(' | ')+']';
    var fullPathFrom = path.join(appRoot, e1consts.PRESETS, 'app');
    var fullPathApp = path.join(appRoot, e1consts.USERAPPS, app.pathName);
    var fullPathTo = path.join(fullPathApp, 'build');
    
    var routesProc = [];
    
    async.series({
        process_forms: function(asyncCallback2) {
            var err=process_forms(forms, app);
            asyncCallback2(err);
        },
        process_menus: function(asyncCallback2) {
            var err=process_menus(menus, forms, routesProc);
            debug('routesProc', routesProc);
            asyncCallback2(err);
        },
        copy: function(asyncCallback2) {
            debug('build copy');
            ncp.ncp(fullPathFrom, fullPathTo, function (err) {
                debug('build copy done', err);
                if (asyncCallback2==null) {
                    return debug('build 1 catch: Caught double call to asyncCallback2 - Buggy ncp?');
                }
                
                if (err) {
                    asyncCallback2(err, false);
                    return console.error(_all_args+' error %s', err);
                }
                
                try {
                    fs.chmodSync(path.join(fullPathTo, 'start'), 0775);
                } catch (err) {
                    console.error('Error ignored when chmod %s', err);
                }

                asyncCallback2(false);
                asyncCallback2 = null;
            });
        },
        packageJson: function(asyncCallback2) {
            var packageJson = path.join(fullPathTo, 'package.json');
            debug('build packageJson', packageJson);
            jsonFile(packageJson, function(err, file) {
                debug('build packageJson done', err);
                if (err) {
                    asyncCallback2(err, false);
                    return console.error(_all_args+' error %s', err);
                }
                
                file.set({
                    name: app.pathName,
                    description: app.doc.name,
                    scripts: {
                        start: 'PORT='+app.doc.port+' nodemon ./bin/www'
                    }
                });
                
                file.save(asyncCallback2);
            });
        },
        buildMenus2Layout: function(asyncCallback2) {
            
            var menu = getItemByName('main', menus);
            debug('build buildMenus2Layout', menu);
            
            if (menu===null) {
                var err = 'Cannot create app without "Main" menu.';
                asyncCallback2(err, false);
                return console.error(_all_args+' error %s', err);
            }
            
            var layoutJade = path.join(fullPathTo, 'views', 'layout.jade');
            var layoutJadeTemplate = fs.readFileSync(layoutJade, {encoding: 'utf8'});
            var layoutJadeTemplateRendered = hogan.compile(layoutJadeTemplate).render({
                links: menu.data
            });
            try {
                fs.writeFileSync(layoutJade, layoutJadeTemplateRendered);
            } catch (err) {
                asyncCallback2(err, false);
                return console.error(_all_args+' error %s', err);
            }
            asyncCallback2(false);
        },
        buildForms2Models: function(asyncCallback2) {
            
            debug('build buildForms2Models');
            var err = build_models(forms, fullPathTo, app.pathName);
            if (err) {
                asyncCallback2(err, false);
                return console.error(_all_args+' error %s', err);
            }
            asyncCallback2(false);
        },
        buildForms2Routes: function(asyncCallback2) {
            
            var err = build_form_routes(forms, fullPathTo);
            if (err) {
                asyncCallback2(err, false);
                return console.error(_all_args+' error %s', err);
            }
            asyncCallback2(false);
        },
        buildForms2Scripts: function(asyncCallback2) {
            //Build basic form scripts
            build_scripts(forms, e1consts.FORMS_PREFIX, e1consts.FORMS_SCRIPT_PARTS.client, e1consts.COMPILE_MODE_CLIENT, fullPathApp, path.join(fullPathTo, 'public', 'javascripts'), asyncCallback2);
        },
        buildFormsModels2Scripts: function(asyncCallback2) {
            //Build model hooks form scripts
            build_scripts(forms, e1consts.FORMS_MODEL_PREFIX, e1consts.FORMS_SCRIPT_PARTS.model, e1consts.COMPILE_MODE_MODULE, fullPathApp, path.join(fullPathTo, 'models'), asyncCallback2);
        },
        buildRoutes2Scripts: function(asyncCallback2) {
            //Build basic route scripts from menus
            build_scripts(menus, e1consts.MENUS_PREFIX, e1consts.MENUS_SCRIPT_PARTS.client, e1consts.COMPILE_MODE_CLIENT, fullPathApp, path.join(fullPathTo, 'public', 'javascripts'), asyncCallback2);
        },
        buildMenus2Routes: function(asyncCallback2) {
            //Build non form routes from menu
            var menu = getItemByName('main', menus);
            debug('build buildMenus2Routes', menu);
            
            if (menu===null) {
                var err = 'Cannot create app without "Main" menu.';
                asyncCallback2(err, false);
                return console.error(_all_args+' error %s', err);
            }
            
            build_other_routes(menu, fullPathApp, fullPathTo, asyncCallback2);
        },
        buildRouter: function(asyncCallback2) {
            //Build router from menu
            var appJs = path.join(fullPathTo, 'app.js');
            var appJsTemplate = fs.readFileSync(appJs, {encoding: 'utf8'});
            var appJsTemplateRendered = hogan.compile(appJsTemplate).render({
                routing: routesProc,
                numlocal: app.doc.numlocal,
                datelocal: app.doc.datelocal,
                randkey: app.doc.randkey
            });
            try {
                fs.writeFileSync(appJs, appJsTemplateRendered);
            } catch (err) {
                asyncCallback2(err, false);
                return console.error(_all_args+' error %s', err);
            }
            asyncCallback2(false);
        },
        buildConfigJson: function(asyncCallback2) {
            debug('build config.json');
            
            var configPart = path.join(appRoot, e1consts.PRESETS, 'parts', 'config.json');
            var configPartTemplate = fs.readFileSync(configPart, {encoding: 'utf8'});
            var configPartCompiled = hogan.compile(configPartTemplate);
            
            //Create config file
            var fullPathConfigDir = path.join(fullPathTo, 'config');
            var fullPathConfig = path.join(fullPathConfigDir, 'config.json');
            var configPartRendered = configPartCompiled.render({
                dialect: app.doc.dbtype,
                username: app.doc.dbusername,
                password: app.doc.dbpassword,
                database: app.doc.dbdatabase,
                host: app.doc.dbhost,
                port: app.doc.dbport,
                storage: './data/'+app.doc.dbdatabase+'.db'
            });
            try {
                fs.mkdirpSync(fullPathConfigDir);
                fs.writeFileSync(fullPathConfig, configPartRendered);
            } catch (err) {
                asyncCallback2(err, false);
                return console.error(_all_args+' error %s', err);
            }
            asyncCallback2(false);
        },
        copyImages: function(asyncCallback2) {
            debug('build copy images');
            var fullPathImagesFrom = path.join(fullPathApp, 'images');
            var fullPathImagesTo = path.join(fullPathTo, 'public', 'images');
            ncp.ncp(fullPathImagesFrom, fullPathImagesTo, function (err) {
                debug('build copy images done', err);
                if (asyncCallback2==null) {
                    return debug('build 1 catch: Caught double call to asyncCallback2 - Buggy ncp?');
                }
                
                if (err) {
                    asyncCallback2(err, false);
                    return console.error(_all_args+' error %s', err);
                }
                
                try {
                    fs.chmodSync(path.join(fullPathTo, 'start'), 0775);
                } catch (err) {
                    console.error('Error ignored when chmod %s', err);
                }

                asyncCallback2(false);
                asyncCallback2 = null;
            });
        }
    },
    callback
    );
};

/**
 * Process forms
 * 
 * @param   {object}        forms       Reference Read forms configs from XLSX
 * @param   {object}        app         Reference App info
 * @return  mixed   False or an error message
 */
function process_forms(forms, app) {
    var form, field;
    app.valids = {};
    
    for (var i = 0, leni = forms.length; i < leni; i++) {
        form = forms[i];
        debug('form.name', form.name);

        form.dbname = e1main.sanitiseLink(form.name);
        form.dbCamname = e1main.camelize(form.dbname);
        form.requires = [];

        for (var j = 0, lenj = form.data.length; j < lenj; j++) {
            field = form.data[j];
            
            field.last = (j>=lenj-1) ? true : false;
            field.dbname = e1main.sanitiseLink(field.Name);
            field.dbCamname = e1main.camelize(field.dbname);
            switch (field.Type.toLowerCase()) {
                case 'text':
                    field.dbtype = "STRING";
                    field.viewtype = 'text';
                    break;
                case 'longtext':
                    field.dbtype = "TEXT('medium')";
                    field.viewtype = 'longtext';
                    break;
                case 'multiline text':
                    field.dbtype = "TEXT('long')";
                    field.viewtype = 'multiline';
                    break;
                case 'integer':
                    field.dbtype = "INTEGER";
                    field.viewtype = 'integer';
                    break;
                case 'decimal':
                    field.dbtype = "DECIMAL(20, 5)";
                    field.viewtype = 'decimal';
                    break;
                case 'yes/no':
                    field.dbtype = "BOOLEAN";
                    field.viewtype = 'boolean';
                    break;
                case 'date':
                    field.dbtype = "DATE";
                    field.viewtype = 'date';
                    break;
                case 'select':
                    field.dbtype = "INTEGER";
                    field.viewtype = 'select';
                    process_formsValids(app, 'select_'+field.Valid);
                    break;
                default:
                    return new Error('Unknown type: '+field.Type);
            }
            
            debug('form.data field', field);
        };
    }
    return false;
}

/**
 * Get valids info (app.valids)
 * 
 * @param {type} app
 * @param {type} valid_type
 * @returns {undefined}
 */
function process_formsValids(app, valid_type) {
    if (valid_type in app.valids) {
        return;
    }
    
    var dataPath = path.join(appRoot, 'data');
    var dataPathJs = path.join(dataPath, valid_type+'.js');
    var dataPathLs = path.join(dataPath, valid_type+'.ls');

    app.valids[valid_type] = {
        type: valid_type,
        isJs: checkFileExists(dataPathJs),
        isLs: checkFileExists(dataPathLs)        
    };
}

/**
 * File exists
 * 
 * @param {type} filepath
 * @returns {checkFileExists.flag|Boolean}
 */
function checkFileExists(filepath) {
    var flag = true;
    try {
        fs.accessSync(filepath, fs.F_OK);
    } catch (e) {
        flag = false;
    }
    return flag;
}

/**
 * Process menus
 * 
 * @param   {object}        menus       Reference Read menus configs from XLSX
 * @param   {object}        forms       Reference Read forms configs from XLSX
 * @param   {array}         routesProc  Add routes to
 * @return  mixed   False or an error message
 */
function process_menus(menus, forms, routesProc) {
    var menu, field, ok;
    
    for (var i = 0, leni = menus.length; i < leni; i++) {
        menu = menus[i];
        menu.dbname = e1main.sanitiseLink(menu.name);
        menu.dbCamname = e1main.camelize(menu.dbname);
        
        for (var j = 0, lenj = menu.data.length; j < lenj; j++) {
            field = menu.data[j];
            field.name = field['Item Name'];
            if (field.Require) {
                field.requires = field.Require.split(" ");        
            }
            
            switch (field.Action) {
                case 'Open form':
                    field.dbname = e1main.sanitiseLink(field['Item Name']);
                    if (false!==(ok=process_menus_form(field, forms))) {
                        return ok;
                    }
                    routesProc.push(field.dbname);
                    break;
                case 'Open own':
                    field.dbname = e1main.sanitiseLink(field['Item Name']);
                    routesProc.push(field.dbname);
                    break;
                case 'Logout':
                    field.dbname = 'global/logout';
                    break;
                default:
                    return new Error('process_menus - Unknown menu type: '+field.Action);
            }
            field.dbCamname = e1main.camelize(field.dbname);
        }
    }
    return false;
}

/**
 * Process menu item with form
 * 
 * @param   {Object}        field       Form field
 * @param   {object}        forms       Reference Read forms configs from XLSX
 * @returns {Error|Boolean}
 */
function process_menus_form(field, forms) {
    var form = getItemByName(field.dbname, forms);
    if (form===null) {
        return new Error('Menu type "Open form" requires a form with the same name: '+field.name);
    }
    
    if (field.Require) {
        var require_item;
        for (var i = 0, len = field.requires.length; i < len; i++) {
            require_item = field.requires[i].toLowerCase();
            if (form.requires.indexOf(require_item)===-1) {
                form.requires.push(require_item);
            }
        }
    }
    
    return false;
}

/**
 * Get item by dbname
 * 
 * @param   {String}        item_dbname     Item dbname
 * @param   {object}        items           Items to get from
 * @return  {object}
 */
function getItemByName(item_dbname, items) {
    //Get item with correct dbname
    var current_item;
    for (var i = 0, len = items.length; i < len; i++) {
        current_item = items[i];
        if (current_item.dbname===item_dbname) {
            return current_item;
        }
    }
    return null;
}

/**
 * Build form routes
 * 
 * @param   {object}        forms       Read forms configs from XLSX
 * @param   {String}        fullPathTo  Full path name
 * @return  mixed           Error or false
 */
function build_form_routes(forms, fullPathTo) {
    //Prepare router file template
    var routerPart = path.join(appRoot, e1consts.PRESETS, 'parts', 'form_router.js');
    var routerPartTemplate = fs.readFileSync(routerPart, {encoding: 'utf8'});
    var routerPartCompiled = hogan.compile(routerPartTemplate);

    for (var i = 0, leni = forms.length; i < leni; i++) {
        var form = forms[i];
        
        //Create router file
        var fullPathRouter = path.join(fullPathTo, 'routes', form.dbname+'.js');
        var routerPartRendered = routerPartCompiled.render({
            name: form.name,
            titleCam: form.dbCamname,
            title: form.dbname,
            fields: form.data,
            requires: form.requires
        });
        try {
            fs.writeFileSync(fullPathRouter, routerPartRendered);
        } catch (err) {
            return err;
        }
    }
    return false;
}

/**
 * Build scripts
 * 
 * @param   {object}        items           Read forms or menus configs from XLSX
 * @param   {String}        prefix          File name prefix
 * @param   {Array}         scriptParts     Script parts
 * @param   {Integer}       compileMode     Script compile mode - One of e1consts.COMPILE_MODE_*
 * @param   {String}        fullPathApp     Full path name of app
 * @param   {String}        fullPathTarget  Full path to name
 * @param   {function}      callback        Callback
 */
function build_scripts(items, prefix, scriptParts, compileMode, fullPathApp, fullPathTarget, callback) {
    var e1scripts = require('./e1scripts'),
        e1compiler = require('./e1compiler');
    
    async.each(items, function(item, callback2) {
        async.waterfall([
            function(callback3) {
                //Load script file
                e1scripts.load(fullPathApp, prefix+item.dbname, function(err, loadedFile) {
                    if (err.allFilesMissing) {
                        callback3(false, false);
                    } else {
                        callback3(err, loadedFile);
                    }
                });
            }, 
            function(loadedFile, callback3) {
                //Compile script file
                if (loadedFile===false) {
                    return callback3(false, '');
                }
                debug('Source script', loadedFile.script.substr(0, 20));
                
                switch(loadedFile.language) {
                    case 'javascript':
                        callback3(false, loadedFile.script);
                        break;
                    case 'livescript':
                        e1compiler.compile(loadedFile.file, loadedFile.script, scriptParts, compileMode, callback3);
                        break;
                    default:
                        callback('Error in build_scripts: Unknown script type '+loadedFile.language);
                }
            }, 
            function(scriptJS, callback3) {
                //Create script file
                scriptName = prefix+item.dbname+'.js';
                fullPathScript = path.join(fullPathTarget, scriptName);
                fs.writeFile(fullPathScript, "//File: /javascripts/"+scriptName+"\n"+scriptJS, callback3); //Write compiled script
            }
        ], callback2);
    }, callback);
}

/**
 * Build other (non form) routes for menu
 * 
 * @param   {object}        menu            Read menu from XLSX
 * @param   {String}        fullPathApp     Full path name of app
 * @param   {String}        fullPathTo      Full path name
 * @param   {function}      callback        Callback
 */
function build_other_routes(menu, fullPathApp, fullPathTo, callback) {
    var e1scripts = require('./e1scripts'),
        e1compiler = require('./e1compiler');
    
    //Prepare router file template
    var routerPart = path.join(appRoot, e1consts.PRESETS, 'parts', 'empty_router.js');
    var routerPartTemplate = fs.readFileSync(routerPart, {encoding: 'utf8'});
    var routerPartCompiled = hogan.compile(routerPartTemplate);
            
    async.each(menu.data, function(field, callback2) {
        debug('build_other_routes field', field);
        switch (field.Action) {
            case 'Open form':
            case 'Logout':
                callback2(false);
                break;
            case 'Open own':
                async.waterfall([
                    function(callback3) {
                        //Load script file
                        e1scripts.load(fullPathApp, e1consts.MENUS_PREFIX_ROUTE+field.dbname, function(err, loadedFile) {
                            if (err.allFilesMissing) {
                                callback3(false, false);
                            } else {
                                callback3(err, loadedFile);
                            }
                        });
                    }, 
                    function(loadedFile, callback3) {
                        //Compile script file
                        if (loadedFile===false) {
                            return callback3(false, '');
                        }
                        debug('Source script', loadedFile.script.substr(0, 20));

                        switch(loadedFile.language) {
                            case 'javascript':
                                callback3(false, loadedFile.script);
                                break;
                            case 'livescript':
                                e1compiler.compile(loadedFile.file, loadedFile.script, [], e1consts.COMPILE_MODE_ROUTE, callback3);
                                break;
                            default:
                                callback('Error in build_scripts: Unknown script type '+loadedFile.language);
                        }
                    }, 
                    function(loadedFileScript, callback3) {
                        debug('openownscript '+field.dbname, loadedFileScript);
                        //Create router file
                        callback3(
                            false, 
                            routerPartCompiled.render({
                                name: field.name,
                                titleCam: field.dbCamname,
                                title: field.dbname,
                                requires: field.requires,
                                routes: loadedFileScript
                            })
                        );
                    }, 
                    function(routerPartCompiled, callback3) {
                        var fullPathRouter = path.join(fullPathTo, 'routes', field.dbname+'.js');
                        fs.writeFile(fullPathRouter, routerPartCompiled, callback3);
                    }
                ], callback2);
                break;
            default:
                callback2(new Error('build_other_routes - Unknown menu type: '+field.Action));
        }
    }, callback);
}

/**
 * Build models
 * 
 * @param   {object}        forms       Read forms configs from XLSX
 * @param   {String}        fullPathTo  Full path name
 * @param   {String}        pathName    Path name
 * @returns {Boolean}
 */
function build_models(forms, fullPathTo, pathName) {
    //Prepare model file template
    var modelPart = path.join(appRoot, e1consts.PRESETS, 'parts', 'model.js');
    var modelPartTemplate = fs.readFileSync(modelPart, {encoding: 'utf8'});
    var modelPartCompiled = hogan.compile(modelPartTemplate);

    var form, fullPathModel, modelPartRendered, field;
    for (var i = 0, leni = forms.length; i < leni; i++) {
        form = forms[i];
 
        debug('Build model', form);
        
        //Create model file
        fullPathModel = path.join(fullPathTo, 'models', form.dbname+'.js');
        modelPartRendered = modelPartCompiled.render({
            titleCam: form.dbCamname,
            title: form.dbname,
            fields: form.data,
            pathName: pathName
        });
        try {
            fs.writeFileSync(fullPathModel, modelPartRendered);
        } catch (err) {
            return err;
        }
    }

    return false;
}