/* 
 * untermStrich auth
 * 
 * @license Private. Use by Christian Koller only
 * @author Christian Koller
 * @copyright 2016 Christian Koller
 */

//@todo const
var SESSION_NAME = 'auth';
//@todo const
var SESSION_VALUE = 'active';
//@todo const
var SESSION_NAME_DATA = 'data';

var http = require('http');
var querystring = require('querystring');

var run = function(method, resolve, reject, data) {
    
    var options = {
        hostname: 'localhost',
        port: 80,
        path: '/versionx1/rest/auth',
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/x-www-form-urlencoded'
        }
    };
    
    switch (method) {
        case 'require':
            if (data.session===SESSION_VALUE) {
                resolve({
                    set_browser_defaults: {
                        staff: parseInt(data.session_data.id) //@todo make name configurable
                    }
                });
            } else {
                resolve({
                    login: true
                });
            }
            break;
        case 'action_login':
            //Prepare query string from data
            var postData = querystring.stringify({
                'username' : data.name.trim(),
                'password' : data.password.trim()
            });
            
            //Set as header length
            options.headers['Content-Length'] = postData.length;
            
            var req = http.request(options, function (res) {
                //Get response
                res.setEncoding('utf8');
                var replyData = ''
                res.on('data', function (chunk) {
                    replyData += chunk;
                });

                res.on('end', function () {
                    console.log('usauth-data', replyData);
                    if (res.statusCode!==200 || replyData.trim()=='FAIL') {
                        //We got anything else or FAIL = FAIL
                        resolve({
                            login_fail: 'Invalid login data'
                        });
                        return;
                    }
                    
                    //We got 200 = OK
                    try {
                        var jsonData = JSON.parse(replyData);
                        console.log('usauth-jsonData', jsonData);
                        
                        if (jsonData.username.trim()!==data.name.trim()) {
                            resolve({
                                login_fail: 'Invalid login reply data'
                            });
                        } else {
                            //Got the right username back
                            resolve({
                                session1: {
                                    key: SESSION_NAME_DATA,
                                    value: jsonData
                                },
                                session2: {
                                    key: SESSION_NAME,
                                    value: SESSION_VALUE
                                },
                                set_browser_defaults: {
                                    staff: parseInt(jsonData.id) //@todo make name configurable
                                },
                                login_done: true
                            });
                        }
                        
                    } catch (err) {
                        resolve({
                            login_fail: 'Invalid login reply '+err
                        });
                    }
                });
            });
            req.on('error', reject);
            req.write(postData);
            req.end();
            
            break;
        case 'config':
            resolve({
                session: SESSION_NAME,
                session_data: SESSION_NAME_DATA
            });
            break;
        default:
            reject('Unknown method "'+method+'"');
    }
}