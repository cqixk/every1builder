### 
# Possible values in list require
###

get_list = (resolve, reject) ->
  reply = values.split ' '
  replyObj = {};
  for val, i in reply
    replyObj[i+1] = val
  resolve replyObj
  return

run = (method, resolve, reject, value) ->
  switch method
    when 'get_value'
      get_list ((values) ->
        resolve values[value]
        return
      ), reject
    when 'get_list'
      get_list resolve, reject
    else
      reject 'Unknown method "' + method + '"'
  return