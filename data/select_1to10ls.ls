### 
# 1to10 select box example in LiveScript
###

get_list = (resolve, reject) ->
  reply = {}
  i = 1
  while i <= 10
    reply[i] = 'Value ' + i
    i++
  resolve reply
  return

run = (method, resolve, reject, value) ->
  switch method
    when 'get_value'
      get_list ((values) ->
        resolve values[value]
        return
      ), reject
    when 'get_list'
      get_list resolve, reject
    else
      reject 'Unknown method "' + method + '"'
  return