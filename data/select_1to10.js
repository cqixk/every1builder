/* 
 * 1to10 select box example
 */

var run = function(method, resolve, reject, value) { 
    switch (method) {
        case 'get_value':
            get_list(function(values) {
                resolve(values[value]);
            }, reject);
            break;
        case 'get_list':
            get_list(resolve, reject);
            break;
        default:
            reject('Unknown method "'+method+'"');
    }
}

function get_list(resolve, reject) {
    var reply = {};
    for (var i = 1;i<=10; i++) {
        reply[i] = 'Value '+i;
    }
    resolve(reply);
}
