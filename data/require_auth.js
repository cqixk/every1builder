/* 
 * auth example
 */

//@todo const
var SESSION_NAME = 'auth';
//@todo const
var SESSION_VALUE = 'active';

var run = function(method, resolve, reject, data) { 
    switch (method) {
        case 'require':
            if (data.session===SESSION_VALUE) {
                resolve({});
            } else {
                resolve({
                    login: true
                });
            }
            break;
        case 'action_login':
            if (data.name==='foo' && data.password==='bar') {
                resolve({
                    session: {
                        key: SESSION_NAME,
                        value: SESSION_VALUE
                    },
                    login_done: true
                });
            } else {
                resolve({
                    login_fail: 'Invalid login data'
                });
            }
            break;
        case 'config':
            resolve({
                session: SESSION_NAME
            });
            break;
        default:
            reject('Unknown method "'+method+'"');
    }
}