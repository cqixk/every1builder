module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        bower_concat: {
            all: {
                dependencies: {
                    'underscore': 'jquery' //NB: Really important for correct load order, otherwise backbone crashes
                },
                dest: {
                    'js': 'presets/app/public/javascripts/bower.js',
                    'css': 'presets/app/public/stylesheets/bower.css'
                }, callback: function (mainFiles, component) {
                    console.log(component);
                    mainFiles.map(function (filepath) {
                        console.log(" - ", filepath);
                    });
                    return mainFiles;
                }, mainFiles: {
                    'bootstrap': ['dist/js/bootstrap.js', 'dist/css/bootstrap.min.css']
                }
            }
        },
        uglify: {
            options: {
                mangle: true,
                compress: true
            }, build: {
                src: 'presets/app/public/javascripts/bower.js',
                dest: 'presets/app/public/javascripts/bower.min.js'
            }
        },
        cssmin: {
            target: {
                files: {
                    'presets/app/public/stylesheets/bower.min.css': ['presets/app/public/stylesheets/bower.css']
                }
            }
        },
        copy: {
            main: {
                files: [
                    // includes files within path
                    {expand: true, cwd: 'bower_components/moment/locale/', src: '**', dest: 'presets/app/public/javascripts/locale/', filter: 'isFile'}
                ]
            }
        }
    });

    grunt.loadNpmTasks('grunt-bower-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-copy');

    // Default task(s).
    grunt.registerTask('default', ['bower_concat', 'uglify', 'cssmin', 'copy']);

};