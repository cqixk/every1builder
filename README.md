#Every1Builder

A DSL and DSM environment for web and mobile web applications simple enough to be useable for non-developers.

##Repo
<https://bitbucket.org/cqixk/every1builder>

##Install 
Ensure you have node 4.4.3 LTS available: <http://nodejs.org/>

    sudo npm install -g bower
    sudo npm install -g pm2
    sudo npm install -g nodemon
    npm install && bower install && grunt

##Start
    npm start

Open http://localhost:3000