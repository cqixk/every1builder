/* global exports, console, setTimeout */

"use strict";

var vm = require('vm'),
    fs = require('fs'),
    async = require('async'),
    path = require('path'),
    appRoot = require('app-root-path').toString(),
    extend = require('util')._extend,
    Type = require('type-of-is');
var debug = require('debug')('app:data');

/**
 * Get display value for value
 * 
 * @param {type} validType
 * @param {type} values
 * @returns {Promise}
 */
exports.get_value = function(validType, values) {
    return call_vm(validType, 'get_value', values);
}

/**
 * Get list of possible values
 * 
 * @param {type} validType
 * @returns {Promise}
 */
exports.get_list = function(validType) {
    return call_vm(validType, 'get_list');
}

/**
 * Load require middleware
 * 
 * @param {type} requireType
 * @param {type} routeTitle
 * @return {Function} middleware
 */
exports.require = function(requireType, routeTitle) {
    return function(req, res, next) {
        debug('Require: ', requireType, ' at ', Date.now());
        
        req.session.requireLastRouteTitle = routeTitle;
        
        async.waterfall([
            function(callback) {
                call_vm('require_'+requireType, 'config').then(function(configList) {
                    callback(null, configList, requireType, req, res);
                }, callback);
            },
            prepare_require,
            function(data, callback) {
                call_vm('require_'+requireType, 'require', data).then(function(actions) {
                    callback(null, actions, requireType, req, res);
                }, callback);
            },
            fulfill_require
        ], function (err, callNext) {
            if (err) {
                console.error(err);
                res.render('error', { error: {status: 'Require middleware error', stack: err} });
            } else if (callNext) {
                next();
            }
        });
    };
}

/**
 * Prepare config values for require
 * 
 * @param {type} configList
 * @param {type} requireType
 * @param {type} req
 * @param {type} res
 * @param {type} callback
 */
function prepare_require(configList, requireType, req, res, callback) {
    debug('Require: ', requireType, ' prepare config ', configList);
    var data = {};
    
    if (!Type.is(configList, 'Object')) {
        callback('Require: '+requireType+' - Missing configList | Got '+Type.string(configList));
    }
    
    var config;
    for (var configKey in configList) {
        config = configList[configKey];
        switch (configKey) {
            case 'session':
                if (!Type.is(config, String)) {
                    callback('Require: '+requireType+' session - Missing String value');
                }
                data.session = req.session[requireType+'_'+config];
                break;
            case 'session_data':
                if (!Type.is(config, String)) {
                    callback('Require: '+requireType+' session_data - Missing String value');
                }
                data.session_data = req.session[requireType+'_'+config];
                break;
            default:
                callback('Require: '+requireType+' is requesting an unknown config type: '+configKey);
        }
    }
    
    debug('Require: ', requireType, ' prepared config ', data);
    callback(false, data);
}

/**
 * Fulfill require actions
 * 
 * @param {type} actions
 * @param {type} requireType
 * @param {type} req
 * @param {type} res
 * @param {type} callback
 */
function fulfill_require(actions, requireType, req, res, callback) {
    debug('Require: ', requireType, ' fulfill ', actions);
    var callNext=true;
    
    if (!Type.is(actions, 'Object')) {
        callback('Require: '+requireType+' - Missing actions | Got '+Type.string(actions));
    }

    var action;
    for (var actionKey in actions) {
        action = actions[actionKey];
        switch (actionKey) {
            case 'login':
                callNext = false;
                res.redirect(303, '/global/login/'+requireType); // See other with GET
                break;
            case 'set_browser_defaults':
                req.browser_defaults = action;
                break;
            default:
                var msg = 'Require: '+requireType+' is requesting an unknown action type: '+actionKey;
                console.error(msg);
                callback(msg);
        }
    }
    
    callback(false, callNext);
}

exports.require_action = function(requireType, method, req, res, values) {
    async.waterfall([
        function(callback) {
            //Get further actions from require
            call_vm('require_'+requireType, 'action_'+method, values).then(function(actions) {
                callback(null, actions, requireType, method, req, res);
            }, callback);
        },
        fulfill_require_action
    ], function (err) {
        if (err) {
            console.error(err);
            res.render('error', { error: {status: 'Require middleware error', stack: err} });
        }
    });
};

/**
 * Fulfill require action actions
 * 
 * @param {type} actions
 * @param {type} requireType
 * @param {type} method
 * @param {type} req
 * @param {type} res
 * @param {type} callback
 * @returns {undefined}
 */
function fulfill_require_action(actions, requireType, method, req, res, callback) {
    debug('Require action: ', requireType, ' ', method,' fulfill ', actions);

    if (!Type.is(actions, 'Object')) {
        callback('Require: '+requireType+' actions - Missing actions | Got '+Type.string(actions));
    }

    var action;
    for (var actionKey in actions) {
        action = actions[actionKey];
        switch (actionKey) {
            case 'session':
            case 'session1':
            case 'session2':
            case 'session3':
                if (!Type.is(action, 'Object')) {
                    callback('Require: '+requireType+' session - Missing main object');
                }
                if (!Type.is(action.key, String)) {
                    callback('Require: '+requireType+' session - Missing key');
                }
                if (!Type.is(action.value, String)) {
                    callback('Require: '+requireType+' session - Missing value');
                }
                req.session[requireType+'_'+action.key] = action.value;
                break;
            case 'login_done':
                if (req.session.requireLastRouteTitle) {
                    res.redirect(303, '/'+req.session.requireLastRouteTitle); // See Other with GET
                } else {
                    res.redirect(303, '/'); // See Other with GET - Fallback only
                }
                break;
            case 'login_fail':
                res.render('login', {
                    requireType: requireType,
                    error: action
                });
                break;
            case 'set_browser_defaults':
                req.browser_defaults = action;
                break;
            default:
                callback('Require action: '+requireType+' '+method+' is requesting an unknown action type: '+actionKey);
        }
    }
    callback(false);
}

var vms = {};

/**
 * Call the VM script (Takes care of loading it)
 * 
 * @param {type} validType
 * @param {type} method
 * @param {type} value
 * @returns {Promise}
 */
function call_vm(validType, method, value) {
    return new Promise(function(resolve, reject) {
        new Promise(function(load_resolve, load_reject) {
            //Promise ensure we have the VM loaded
            if (vms.hasOwnProperty(validType)) {
                load_resolve(vms[validType]);
            } else {
                load_vm(validType, load_resolve, load_reject);
            }
        }).then(function(vmType) {
            //Execute VM script
            try {
                vmType.run(method, resolve, reject, value);
            } catch (err) {
                console.error('Error in VM call to '+method+': '+err);
                reject('Error in VM call to '+method+': '+err);
            }
        }, reject);
    });
}

/**
 * Load the VM script
 * 
 * @param {type} type
 * @param {type} resolve
 * @param {type} reject
 * @returns {undefined}
 */
function load_vm(type, resolve, reject) {
    var e1scriptsPath = path.join(appRoot, 'lib', 'e1scripts.js');
    var e1scripts = require(e1scriptsPath);
    
    async.waterfall([
        function(callback) {
            //Get valid script path
            var dataPath = path.join(appRoot, 'data');
            
            e1scripts.load(dataPath, type, callback);
        },
        function(loadedFile, callback) {
            //Run script
            debug('Run script', loadedFile.script.substr(0, 20));
            try {
                //Init VM context
                var context = new vm.createContext({
                    console: console,
                    require: require,
                    setTimeout: setTimeout
                });
                
                //Init VM script
                var contextScript;
                switch(loadedFile.language) {
                    case 'javascript':
                        contextScript = new vm.Script(loadedFile.script, {filename: loadedFile.file});
                        break;
                    case 'livescript':
                        var LiveScript = require('livescript');
                        var lsOptions = {
                            bare: true, //if true, compile without the top-level function wrapper
                            header: false //if true, add the "Generated by" header
                        };
                        contextScript = new vm.Script(LiveScript.compile(loadedFile.script, lsOptions));
                        break;
                    default:
                        callback('Error in VM run - load_vm: Unknown script type '+loadedFile.language);
                }
                
                //Run the VM script to init the context
                contextScript.runInContext(context);
                
                //Assign to vms array and return
                vms[type] = context;
                callback(false, vms[type]);
            } catch (err) {
                callback('Error in VM run - load_vm: '+err);
            }
        }
    ], function (err, result) {
        if (err) {
            console.error(err);
            reject(err);
        } else {
            resolve(result);
        }
    });
}