/* global exports */

"use strict";

var e1data = require('./e1data');

/**
 * Prepare cell to output
 * 
 * @param   {object}    req         Request
 * @param   {string}    type        Type
 * @param   {string}    valid       Valid information
 * @param   {string}    value       DB Value
 * @param   {function}  callback    Callback
 */
exports.cell_to_out = function(req, type, valid, value, callback) {
    var reply = {
        'plain': value,
        'type': type
    };
    
    switch (type) {
        case 'text':
        case 'longtext':
        case 'multiline':
        case 'button':
        case 'integer':
        case 'boolean':
            reply.edit = value;
            callback(false, reply);
            break;
        case 'decimal':
            reply.edit = req.app.locals.numbro(value).format('0,0.0[0000]');
            callback(false, reply);
            break;
        case 'date':
            var date = req.app.locals.moment(value);
            if (date.isValid()) {
                reply.edit = date.format('L HH:mm');
                reply.order = date.format('X');
            } else {
                reply.edit = '';
                reply.order = '';
            }
            callback(false, reply);
            break;
        case 'select':
            e1data.get_value('select_'+valid, value).then(function(e1data_reply) {
                reply.edit = value;
                reply.table = e1data_reply;
                callback(false, reply);
            }, function(err) {
                callback(err, false);
            });
            break;
        default:
            var msg = 'Unknown type: '+type;
            console.error(msg);
            callback(msg, false);
    }
};

/**
 * Prepare input value to db value
 * 
 * @param   {object}    req     Request
 * @param   {string}    type    Type
 * @param   {string}    name    Name of input value
 * @returns {nm$_e1form.exports.in_to_db.value|exports.in_to_db.value|String|Boolean}
 */
exports.in_to_db = function(req, type, name) {
    var value = req.body[name] ? req.body[name] : '';
    
    switch (type) {
        case 'text':
        case 'longtext':
        case 'multiline':
            return value;
        case 'integer':
        case 'button':
            if (value!=='') {
                return parseInt(value.trim());
            } else {
                return null;
            }
        case 'boolean':
            return (value===true || value==='true') ? true : false;
        case 'decimal':
            if (value!=='') {
                return parseFloat(req.app.locals.numbro().unformat(value.trim()));
            } else {
                return null;
            }
            break;
        case 'date':
            if (value!=='') {
                var date = req.app.locals.moment(value.trim(), 'L HH:mm');
                if (date.isValid()) {
                    return date.format();
                } else {
                    return null;
                }
            } else {
                return null;
            }
            break;
        case 'select':
            if (value!=='') {
                return parseInt(value.trim());
            } else {
                return null;
            }
        default:
            console.error('Unknown type: '+type);
            return null;
    }
};