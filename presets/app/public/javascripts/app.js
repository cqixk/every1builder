/* global Backform, _, datelocal, BootstrapDialog */

/**
 * Prepare table columns
 * 
 * @type table_columns_prepare
 */
var table_columns_prepare = (function () {
    function table_columns_prepare(form_fields) {
        /* Create columns to render */
        var columns = [
            {
                "data": "id",
                "render": {
                    "_": ".plain",
                    "display": table_cell_render
                },
                "width": "45px"
            }
        ];

        /* Add fields to columns */
        for (var i = 0, leni = form_fields.length, form_field; i < leni; i++) {
            form_field = form_fields[i];
            columns.push({
                "data": form_field.dbname,
                "render": {
                    "_": ".plain",
                    "display": table_cell_render
                },
                "defaultContent": '<small class="glyphicon glyphicon-ban-circle text-muted" title="Not set" aria-hidden="true"></small>'
            });
        }

        return columns;
    }

    //Render table cell
    var table_view_button = null,
            table_view_boolean_true,
            table_view_boolean_false,
            table_view_text,
            table_view_textlong;
    function table_cell_render(data) {
        //Init on first prepare
        if (table_view_button === null) {
            table_view_button = _.template($("#table-view-button").html());
            table_view_boolean_true = $("#table-view-boolean-true").html();
            table_view_boolean_false = $("#table-view-boolean-false").html();
            table_view_text = _.template($("#table-view-text").html());
            table_view_textlong = _.template($("#table-view-textlong").html());
        }

        switch (data.type) {
            case 'text':
            case 'longtext':
            case 'multiline':
                if (data.edit && data.edit.length > 20) {
                    data.displayshort = data.edit.substr(0, 20) + '...';
                    return table_view_textlong(data);
                } else {
                    return table_view_text(data);
                }
            case 'button':
                return table_view_button(data);
            case 'integer':
            case 'decimal':
            case 'date':
                return data.edit;
            case 'select':
                return data.table;
            case 'boolean':
                if (data.plain) {
                    return table_view_boolean_true;
                } else {
                    return table_view_boolean_false;
                }
            default:
                return 'Unknown type ' + data.type;
        }
    }

    return table_columns_prepare;
})();

/**
 * Prepare form fields
 * 
 * @type form_fields_prepare
 */
var form_fields_prepare = (function () {
    function form_fields_prepare(form_fields, number_delimiters, form_valids) {
        var reply = [];
        for (var i = 0, leni = form_fields.length, form_field; i < leni; i++) {
            form_field = form_fields[i];

            reply.push(form_field_prepare(form_field, number_delimiters, form_valids));
        }
        return reply;
    }

    /**
     * Prepare a single field
     * 
     * @param {type} field
     * @param {type} number_delimiters
     * @param {type} form_valids
     * @returns {object}
     */
    function form_field_prepare(field, number_delimiters, form_valids) {
        var disabled = (field.disabled) ? true : false;
        switch (field.type) {
            case 'text':
                return {
                    name: field.dbname,
                    label: field.name,
                    control: 'input-size',
                    gridcols: 6,
                    disabled: disabled
                };
            case 'longtext':
                return {
                    name: field.dbname,
                    label: field.name,
                    control: 'input',
                    maxlength: 9999,
                    disabled: disabled
                };
            case 'integer':
                return {
                    name: field.dbname,
                    label: field.name,
                    control: 'input-size',
                    gridcols: 4,
                    type: 'number',
                    disabled: disabled
                };
            case 'decimal':
                return {
                    name: field.dbname,
                    label: field.name,
                    control: 'input-size',
                    gridcols: 4,
                    placeholder: '1'+number_delimiters.thousands+'234'+number_delimiters.decimal+'56',
                    disabled: disabled
                };
            case 'date':
                return {
                    name: field.dbname,
                    label: field.name,
                    control: 'input-date',
                    disabled: disabled
                };
            case 'multiline':
                return {
                    name: field.dbname,
                    label: field.name,
                    control: 'textarea',
                    maxlength: 9999,
                    disabled: disabled
                };
            case 'boolean':
                return {
                    name: field.dbname,
                    label: field.name,
                    control: 'checkbox',
                    disabled: disabled
                };
            case 'select':
                return {
                    name: field.dbname,
                    label: field.name,
                    control: 'select',
                    options: get_valid(form_valids, field.valid),
                    disabled: disabled
                };
            case 'button':
                //Not in use for "id"
                return {
                    name: field.dbname,
                    control: 'input-hidden',
                    disabled: disabled
                };
            default:
                return {
                    name: field.dbname,
                    label: 'Unknown type ' + field.type,
                    control: 'help'
                };
        }
    }
    
    /**
     * Get on of the valid types
     * 
     * @param {type} form_valids
     * @param {type} valid_type
     * @returns {unresolved}
     */
    function get_valid(form_valids, valid_type) {
        var reply = _.map(form_valids[valid_type], function (val, key) {
            return {
                label: val,
                value: parseInt(key)
            };
        });
        reply.unshift({
            label: '',
            value: ''
        });
        
        return reply;
    }

    return form_fields_prepare;
})();

$(document).ready(function () {
    /* My own control styles */
    var extend_backform = (function () {
        var input_input_size = _.template($("#input-input-size").html());
        var input_input_date = _.template($("#input-input-date").html());
        
        _.extend(Backform, {
            groupClassName: "form-group row", //row to get height for rows
            InputHiddenControl: Backform.InputControl.extend({
                defaults: {},
                template: _.template('<input type="hidden" name="<%=name%>" value="<%-value%>" />')
            }),
            InputSizeControl: Backform.InputControl.extend({
                defaults: {
                    type: "text",
                    label: "",
                    gridcols: 8,
                    maxlength: 255,
                    extraClasses: [],
                    helpMessage: ''
                },
                template: input_input_size
            }),
            InputDateControl: Backform.InputControl.extend({
                defaults: {
                    label: "",
                    gridcols1: 4,
                    gridcols2: 4,
                    maxlength1: 125,
                    maxlength2: 125,
                    placeholder1: moment().format('L'),
                    placeholder2: '12:34',
                    extraClasses1: [],
                    extraClasses2: []
                },
                template: input_input_date,
                getValueFromDOM: function() {
                    var date = this.$el.find(".val1").data("DateTimePicker").date();
                    if (date===null || !date.isValid()) {
                        return null;
                    }
                    
                    return this.formatter.toRaw(date.format('L')+' '+this.$el.find("input.val2").val(), this.model);
                },
                render: function() {
                    Backform.InputControl.prototype.render.apply(this, arguments);
                    
                    var $input, val;
                    
                    //Init date input
                    $input = this.$el.find(".val1");
                    val = moment($input.find("input").val(), 'L'); //Save input before datetimepicker init
                    $input.datetimepicker({
                        locale: datelocal,
                        format: 'L'
                    });
                    $input.data("DateTimePicker").date(val); //Set saved value
                    return this;
                },
                events: {
                    "dp.change .val1": "onChange",
                    "change .val2": "onChange",
                    "focus input": "clearInvalid"
                }
            })
        });
    })();
});

/**
 * Call a user defined function (on_...)
 * 
 * @param   {String}    name    Function name
 * @param   mixed       values  Values
 * @returns mixed       Error or return value of function
 */
function call_user_func(name, values) {
    try {
        var fn = window[name];
        if (typeof fn === "function") {
            return fn(values);
        } else {
            throw 'Cannot find '+name+' to call';
        }
    } catch (err) {
        bootstrap_alert(err, name+' error');
        return new Error(name+' error: '+err);
    }
}

/**
 * Show alert message
 * 
 * @param   {String}    message     Message
 * @param   {String}    title       Optional title
 */
function bootstrap_alert(message, title) {
    if (window.console && console.log) {
        console.log('bootstrap_alert', message);
    }
    BootstrapDialog.alert({
        title: title || 'Error',
        message: message,
        type: BootstrapDialog.TYPE_DANGER,
        closable: true,
        draggable: true
    });
}