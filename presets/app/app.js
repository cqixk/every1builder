/* global __dirname, module */

"use strict";

var express = require('express');
var expressValidator = require('express-validator');
var session = require('express-session');
var SQLiteStore = require('connect-sqlite3')(session);
var helmet = require('helmet');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var packageJson = require('./package.json');

// Routes
var rIndex = require('./routes/index');
var rGlobal = require('./routes/global');
{{#routing}}
    var r{{.}} = require('./routes/{{.}}');
{{/routing}}

var app = express();

// Security
app.use(helmet());

// Favicon
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

// Session
app.use(session({
    store: new SQLiteStore({
        dir: './data/'
    }),
    secret: '{{randkey}}',
    resave: false,
    saveUninitialized: false
}));

// View engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// An auto archived log stream
var accessLogStream = require("stream-file-archive")({
    path: "logs/app-%Y-%m-%d.log", // Write logs rotated by the day
    symlink: "logs/current.log", // Maintain a symlink called current.log
    compress: true, // Gzip old log files
});

app.use(logger('combined', {stream: accessLogStream}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(expressValidator());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/fonts', express.static(path.join(__dirname, '..', '..', '..', 'bower_components', 'bootstrap', 'dist', 'fonts')));

// Global view vars
app.locals.title = packageJson.description;
app.locals.titleMain = packageJson.description;
app.locals.datelocal = '{{datelocal}}';
app.locals.numbro = require('numbro');
app.locals.numbro.culture('{{numlocal}}');
app.locals.moment = require('moment');
app.locals.moment.locale('{{datelocal}}');

// Routing
app.use('/', rIndex);
app.use('/global', rGlobal);
{{#routing}}
    app.use('/{{.}}', r{{.}});
{{/routing}}

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
