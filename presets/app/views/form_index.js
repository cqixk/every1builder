/* global number_delimiters, table_cell_render, form_fields, Backbone, Backform, form_valids, form_defaults */

$(document).ready(function () {
    var edit_data_item; //Current edit data item
    var $edit_data_row; //Current edit data table row
    var $edit_data_table; //The table
    var data_url = document.location.href + '/data';

    /* Prepare table columns */
    var prepared_columns = table_columns_prepare(form_fields);

    /* Render the DataTable */
    $edit_data_table = $('table#data-table').DataTable({
        "language": number_delimiters,
        "processing": true,
        "ajax": {
            "url": data_url,
            "type": "GET"
        },
        "columns": prepared_columns,
        "deferRender": true,
        "columnDefs": [
            {"orderable": false, "targets": 0}
        ],
        "dom": "<'row'<'col-sm-6'<'#data-table-toolbar'>><'col-sm-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-3'i><'col-sm-3'l><'col-sm-6'p>>",
        "lengthMenu": [[10, 20, 50, 100, 200, -1], [10, 20, 50, 100, 200, "All"]]
    })
    .on('draw.dt', function () {
        /* Tooltip for longtext */
        $('span.tooltipitem').tooltip({placement: 'top'});
    });
    
    /* Delete button within DataTable click */
    $edit_data_table.on('click', '.edit-delete', function(e) {
        $edit_data_row = $(this).parents('tr');
        var table_data = $edit_data_table.row($edit_data_row).data();
        
        if (confirm('Delete row?')) {
            var bd = $('<div class="modal-backdrop"></div>');
            //Send to server
            $.ajax({
                    dataType: 'json',
                    type: 'DELETE',
                    url: data_url+'/'+table_data.id.plain,
                    timeout:8000 //8 second timeout
                })
                .done(function () {
                    $edit_data_table.row($edit_data_row).remove().draw();
                })
                .fail(function (xhr, textStatus) {
                    if (textStatus === 'timeout') {
                        bootstrap_alert('Delete timed out. Please refresh page!', 'Communication failed');
                    } else {
                        bootstrap_alert(xhr.responseText);
                    }
                })
                .always(function () {
                    bd.remove();
                });
        }
    });

    /* Load data on edit click */
    $('#edit-modal').on('show.bs.modal', function (e) {
        //Get fields info
        var fields_prepared = form_fields_prepare(form_fields, number_delimiters, form_valids);
        if (window.console && console.log) {
            console.log('fields_prepared', fields_prepared);
        }

        //Get target information
        var $relatedTarget = $(e.relatedTarget);
        if ($relatedTarget.attr('id')==='data-table-add') {
            //Create
            $edit_data_row = null;
            var table_data = $.extend({}, form_defaults);
            edit_data_item = new Backbone.Model(table_data);
        } else {
            //Update
            $edit_data_row = $relatedTarget.parents('tr');
            var table_data = $edit_data_table.row($edit_data_row).data();
            var table_data_prep = _.mapObject(table_data, 'edit');
            edit_data_item = new Backbone.Model(table_data_prep);
        }
        
        //Render the Backform
        new Backform.Form({
            el: $("#edit-modal-form-fields"),
            fields: fields_prepared,
            model: edit_data_item
        }).render();
    });

    /* Move button to DataTable toolbar */
    $('#data-table-add').detach().appendTo('#data-table-toolbar');

    /* Bind save event */
    $("#edit-modal-form").on("submit", function(e) {
        //Call user func
        var curr = edit_data_item.toJSON();
        var reply = call_user_func('on_save', curr);
        if (reply instanceof Error) {
            return false;
        } else {
            edit_data_item.set(curr);
        }
        
        var data_id=edit_data_item.get('id');
        var xhr = {
            data: edit_data_item.toJSON(),
            dataType: 'json'
        };
        if (data_id) {
            //PUT Update
            xhr.type = 'PUT';
            xhr.url = data_url+'/'+data_id;
        } else {
            //POST Create
            xhr.type = 'POST';
            xhr.url = data_url;
        }
        
        //Send to server
        $.ajax(xhr)
            .done(function (data) {
                $('#edit-modal').modal('hide');
                if ($edit_data_row) {
                    $edit_data_table.row($edit_data_row).data(data);
                } else {
                    $edit_data_table.row.add(data).draw();
                }
            })
            .fail(function (xhr) {
                bootstrap_alert(xhr.responseText);
            });
        
        //Ignore default
        return false;
    });
});