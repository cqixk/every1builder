var express = require('express');
var router = express.Router();

/* GET no global main page. */
router.get('/', function(req, res, next) {
    res.sendStatus(404);
});

/* GET Login page. */
router.get('/login/:requireType', function(req, res, next) {
    req.checkParams('requireType', 'Invalid require type').notEmpty().withMessage('Require type is required').isAscii();
    var errors = req.validationErrors();
    if (errors) {
        res.render('error', { error: {status: 'Validation error', validation: errors} }); return;
    }

    res.render('login', {
        requireType: req.params.requireType
    });
});

/* POST Do login. */
router.post('/login', function(req, res, next) {
    req.sanitize('name').trim();
    req.checkBody('requireType', 'Invalid require type').notEmpty().withMessage('Require type is required').isAscii();
    req.checkBody('name', 'Name is required').notEmpty();
    req.checkBody('password', 'Password is required').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        res.render('error', { error: {status: 'Validation error', validation: errors} }); return;
    }
    
    var e1data = require('../lib/e1data');
    e1data.require_action(req.body.requireType, 'login', req, res, {name: req.body.name, password: req.body.password});
});

/* GET Do logout. */
router.get('/logout', function(req, res, next) {
    req.session.destroy(function(err) {
        if (err) {
            console.error(err);
            res.render('error', { error: {status: 'Logout error', stack: err} }); return;
        }
        res.redirect(307, '/'); // Temporary Redirect
    });
});

module.exports = router;
