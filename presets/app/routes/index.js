var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', {
        'pageScript': 'menus_main',
        'current': req.session.current
    });
});

module.exports = router;
