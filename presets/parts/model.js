'use strict';

var forms_hooks = require('./forms_model_{{title}}')

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('{{titleCam}}', {
      {{#fields}}
        {{dbname}}: DataTypes.{{{dbtype}}}{{^last}}, {{/last}}
      {{/fields}}
    }, {
        tableName: '{{pathName}}_{{title}}',
        classMethods: {
            associate: function (models) {
                // associations can be defined here
            }
        },
        hooks: {
            beforeValidate: forms_hooks.on_before,
            beforeCreate: forms_hooks.on_beforecreate,
            beforeDestroy: forms_hooks.on_beforedestroy,
            beforeUpdate: forms_hooks.on_beforeupdate,
            afterCreate: forms_hooks.on_aftercreate,
            afterDestroy: forms_hooks.on_afterdestroy,
            afterUpdate: forms_hooks.on_afterupdate
        }
    });
};