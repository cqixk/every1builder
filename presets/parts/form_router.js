var express = require('express'),
    async = require('async'),
    debug = require('debug')('app:{{titleCam}}');
var router = express.Router();
var models = require('../models');
var e1form = require('../lib/e1form');
var e1data = require('../lib/e1data');

"use strict";

var forms_hooks = require('../models/forms_model_{{title}}')

{{#requires}}
    //Require {{.}}
    router.use(e1data.require('{{.}}', '{{title}}'));
{{/requires}}

/* GET home page. */
router.get('/', function(req, res, next) {
    //Set current page info
    var sess = req.session;
    sess.current = '{{name}}';
    
    prepare_valids(req, function(err, valids) {
        if (err) {
            console.error('GET / prepare_valids', err);
            res.render('error', { error: {status: 'Prepare error', stack: err} }); return;
        }

        res.render('form_index', {
            'title': '{{name}}',
            'pageScript': 'forms_{{title}}',
            'fields': get_fields_list(req),
            'valids': valids,
            'browser_defaults': (req.browser_defaults) ? req.browser_defaults : {}
        });
    });
    
});

/* GET data. */
router.get('/data', function(req, res, next) {
    var where = {};
    forms_hooks.on_findall(where, req);
    
    debug('GET /data where', where);
    
    models.{{titleCam}}.findAll({where: where}).then(function (rows) {
        var prepare_output_row = prepare_output_row_maker(req);
        async.map(
            rows, 
            prepare_output_row, 
            function(err, data){
                if (err) {
                    console.error('GET /data prepare_output_row', err);
                    res.json({ error: err, data: [] }); return;
                }
                res.json({ data: data });
            }
        );
    }, function (err) { 
        console.error('GET /data', err);
        res.json({ error: err, data: [] });
    });
});

/* POST Create data. */
router.post('/data', function(req, res, next) {
    //Parse data in
    var data = prepare_db_row(req);
    debug('POST Create data', data);
    
    models.{{titleCam}}.create(data).then(function(result) {
        debug('Got result', result);
        var prepare_output_row = prepare_output_row_maker(req);
        prepare_output_row(result, function(err, reply) {
            if (err) {
                console.error('POST /data prepare_output_row', err);
                res.json({ error: err }); return;
            }
            res.json(reply)
        });
    }, function (error) { 
        console.error('POST /data', error);
        res.status(500).json({ error: {status: 'Error saving {{titleCam}} data', error: error} });
    });
});

/* PUT Update data. */
router.put('/data/:itemId', function(req, res, next) {
    req.checkParams('itemId', 'Invalid ID').notEmpty().withMessage('ID is required').isInt();
    var errors = req.validationErrors();
    if (errors) {
        res.render('error', { error: {status: 'Validation error', validation: errors} }); return;
    }
    
    //Parse data in
    var data = prepare_db_row(req);
    debug('PUT Update data', data);
    
    models.{{titleCam}}.update(
        data,
        {
            where: { 'id' : req.params.itemId }
        }
    ).then(function() {
        debug('Update done');
        //Get values after update
        models.{{titleCam}}.findById(req.params.itemId).then(function(result) {
            debug('Got result with find', result);
            var prepare_output_row = prepare_output_row_maker(req);
            prepare_output_row(result, function(err, reply) {
                if (err) {
                    console.error('PUT /data/:itemId prepare_output_row', err);
                    res.json({ error: err }); return;
                }
                res.json(reply)
            });
        }, function (error) { 
            console.error('PUT /data/:itemId findById', error);
            res.status(500).json({ error: {status: 'Error reading {{titleCam}} after save', error: error} });
        });
    }, function (error) { 
        console.error('PUT /data/:itemId', error);
        res.status(500).json({ error: {status: 'Error saving {{titleCam}} data', error: error} });
    });
});

/* DELETE data. */
router.delete('/data/:itemId', function(req, res, next) {
    req.checkParams('itemId', 'Invalid ID').notEmpty().withMessage('ID is required').isInt();
    var errors = req.validationErrors();
    if (errors) {
        res.render('error', { error: {status: 'Validation error', validation: errors} }); return;
    }
    debug('DELETE data', req.params.itemId);
    
    models.{{titleCam}}.findById(req.params.itemId).then(function(result) {
        result.destroy()
        .then(function(){
            res.json('done');
        })
    }, function (error) { 
        console.error('DELETE /data/:itemId', error);
        res.status(500).json({ error: {status: 'Error deleting {{titleCam}}', error: error} });
    });
});

/**
 * Prepare (for) database row
 * 
 * @param   {object}    req     Request
 * @returns {nm$_form_router.in_to_db.result}
 */
function prepare_db_row(req) {
    var result = {};
    var fields = get_fields_list(req), field;
    for (var i = 0, leni = fields.length; i < leni; i++) {
        field = fields[i];
        if (field.dbname!=='id') {
            result[field.dbname] = e1form.in_to_db(req, field.type, field.dbname);
        }
    }
    return result;
}

/**
 * Prepare (for) output row - Maker
 * 
 * @param   {object}    req         Request
 */
function prepare_output_row_maker(req) {
    var fields = get_fields_list(req);
    fields.unshift({
        'name': 'id',
        'dbname': 'id',
        'type': 'button', //Edit type
        'viewtype': 'button',
        'valid': ''
    });
    
    /**
     * Prepare (for) output row
     * 
     * @param   {array}     row         Data row
     * @param   {function}  callback    Callback
     */
    return function(row, callback) {
        var reply = {};
        async.each(fields, function(field, asyncCallback) {
            e1form.cell_to_out(req, field.viewtype, field.valid, row[field.dbname], function(err, outInfo) {
                reply[field.dbname] = outInfo;
                asyncCallback(err);
            });
        }, function(err) {
            callback(err, reply);
        });
    };
}

/**
 * Prepare valids
 * 
 * @param   {object}    req         Request
 * @param   {function}  callback    Callback
 */
function prepare_valids(req, callback) {
    var valids = {};
    var fields = get_fields_list(req), field;
    for (var i = 0, leni = fields.length; i < leni; i++) {
        field = fields[i]
        if (field.type==='select') {
            valids[field.valid] = {};
        }
    }
    debug('valids unprep', valids);
    
    async.forEachOf(
        valids, 
        function(item, key, asyncCallbackMap) {
            e1data.get_list('select_'+key).then(function(reply) {
                debug('Valids for ', key, ': ', reply)
                valids[key] = reply;
                asyncCallbackMap();
            }, function(err) {
                asyncCallbackMap(err);
            });
        }, function (err) {
            if (err) {
                callback(err, false);
            } else {
                debug('valids prep', valids);
                callback(false, valids);
            }
        }
    );
}

/**
 * Returns field list
 * 
 * @param   {object}    req         Request
 * @return  {Array}     Field list
 */
function get_fields_list(req) {
    var fields = [
        {{#fields}}
         {
            'name': '{{Name}}',
            'dbname': '{{dbname}}',
            'type': '{{viewtype}}', //Edit type
            'viewtype': '{{viewtype}}',
            'valid': '{{Valid}}'
         }{{^last}}, {{/last}}
        {{/fields}}
    ];
    
    //Hook fieldslist
    for (var i = 0, leni = fields.length; i < leni; i++) {
        forms_hooks.on_fieldslist(fields[i], req);
    }

    return fields;
}

module.exports = router;
