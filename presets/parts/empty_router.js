var express = require('express'),
    async = require('async'),
    debug = require('debug')('app:{{titleCam}}');
var router = express.Router();
var e1data = require('../lib/e1data');

{{#requires}}
    //Require {{.}}
    router.use(e1data.require('{{.}}', '{{title}}'));
{{/requires}}

{{{routes}}}
        
router.all('*', function(req, res, next){
    res.render('error', { error: {status: 'Error: Route not defined by user'} }); return;
});

module.exports = router;
