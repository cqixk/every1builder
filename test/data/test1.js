/* Some global functions */
var call, change, on_load, on_undefined, on_false;
call = function(func){
  return func;
};
change = function(key, value){
  return $(key).text(value);
};
/* END */
on_load = function(values){
  change('h1', 'Welcome to RealArch');
};
on_undefined = function(values){};
on_false = function(values){};