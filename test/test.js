var assert = require('assert');
var e1consts = require('../lib/e1consts'),
    e1compiler = require('../lib/e1compiler');
var fs = require('fs'),
    path = require('path');

describe('e1compiler', function () {
    it('should compile file ls', function () {
        var sourcePathFrom = path.resolve(__dirname, 'data/test1.ls');
        var sourceFrom = fs.readFileSync(sourcePathFrom, 'utf8');
        var sourceCompare = fs.readFileSync(path.resolve(__dirname, 'data/test1.js'), 'utf8');
        
        e1compiler.compile(sourcePathFrom, sourceFrom, e1consts.FORMS_SCRIPT_PARTS.client, e1consts.COMPILE_MODE_CLIENT, function(err, compiledSource) {
            assert.equal(false, err);
            assert.equal(sourceCompare, compiledSource);
        });
    });
});
