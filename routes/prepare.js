var express = require('express');
var router = express.Router();
var debug = require('debug')('Every1Builder:server');

/* GET Prepare. */
router.get('/:pathName', function(req, res, next) {
    req.checkParams('pathName', 'Invalid path name').notEmpty().withMessage('Path name is required').isAlphanumeric().isLength(2, 100);
    var errors = req.validationErrors();
    if (errors) {
        res.render('error', { error: {status: 'Validation error', validation: errors} }); return;
    }
    
    //Prepare
    var e1apps = require('../lib/e1apps');
    e1apps.prepare(req.params.pathName, function(err, app, steps) {
        if (err) {
            res.render('error', { error: {status: 'Error', stack: [err], steps: steps} }); return;
        }
        debug('Prepared app', app);
        res.render('prepare', {app: app, steps: steps});
    });
});

/* GET Open. */
router.get('/open/:pathName', function(req, res, next) {
    req.checkParams('pathName', 'Invalid path name').notEmpty().withMessage('Path name is required').isAlphanumeric().isLength(2, 100);
    var errors = req.validationErrors();
    if (errors) {
        res.status(500).json({ error: {status: 'Validation error', validation: errors} }); return;
    }
    
    var e1main = require('../lib/e1main');
    e1main.openInfo_GetBase(req.params.pathName, function(err, app) {
        if (err) {
            res.status(500).json({ error: {status: 'Error', error: err} }); return;
        }
        
        try {
            var open = require("open");
            debug('Opening '+app.folderPath);
            open(app.folderPath, function (err) {
                if (err) {
                    res.status(500).json({ error: {status: 'Error opening folder', error: err} });
                } else {
                    res.send('done');
                }
            });
        } catch (err) {
            res.status(500).json({ error: {status: 'Error prepare opening folder', error: err} });
        }
    });
});

module.exports = router;
