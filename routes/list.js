var express = require('express');
var router = express.Router();
var XLSX = require('xlsx'),
    util = require('util');

/* GET Show list. */
router.get('/', function(req, res, next) {
    var e1apps = require('../lib/e1apps');
    e1apps.list(function(err, list) {
        if (err) {
            res.render('error', { error: {status: 'Error', stack: [err]} }); return;
        }
        res.render('list', {
            Applist: list
        });
    });
});

module.exports = router;
