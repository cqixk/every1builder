var express = require('express');
var router = express.Router();
var portfinder = require('portfinder');
var debug = require('debug')('Every1Builder:server');

/* GET Form. */
router.get('/', function(req, res, next) {
    var steps = ['Init create'];
    portfinder.basePort = 3001;
    portfinder.getPort(function (err, port) {
        if (err) {
            res.render('error', { error: {status: 'Error', stack: [err], steps: steps} }); return;
        }
        res.render('create', {port: port});
    });
});

/* POST Create. */
router.post('/do', function(req, res, next) {
    req.sanitize('name').trim();
    req.checkBody('name', 'Invalid name').notEmpty().withMessage('Name is required').isLength(2, 100);
    req.checkBody('port', 'Invalid port (80-65535)').notEmpty().withMessage('Port is required').isInt({ min: 80, max: 65535 });
    var errors = req.validationErrors();
    if (errors) {
        res.render('error', { error: {status: 'Validation error', validation: errors} }); return;
    }
    
    //Create
    var e1apps = require('../lib/e1apps');
    e1apps.create(req.body.name, parseInt(req.body.port), function(err, pathName) {
        if (err) {
            res.render('error', { error: {status: 'Error', stack: [err]} }); return;
        }
        debug('Created app', pathName);
        res.redirect('/list');
    });
});

module.exports = router;
