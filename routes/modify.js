var express = require('express');
var router = express.Router();
var debug = require('debug')('Every1Builder:server');

/* GET Form. */
router.get('/:pathName', function(req, res, next) {
    req.checkParams('pathName', 'Invalid path name').notEmpty().withMessage('Path name is required').isAlphanumeric().isLength(2, 100);
    var errors = req.validationErrors();
    if (errors) {
        res.render('error', { error: {status: 'Validation error', validation: errors} }); return;
    }
    
    //Show edit form
    var e1main = require('../lib/e1main');
    var dbtypes = require(__dirname + '/../config/dbtypes.json');
    var numlocals = require(__dirname + '/../config/numlocals.json');
    var datelocals = require(__dirname + '/../config/datelocals.json');
    e1main.openInfo_GetBase(req.params.pathName, function(err, app) {
        if (err) {
            res.render('error', { error: {status: 'Error', stack: [err]} }); return;
        }
        res.render('modify', {
            app: app,
            dbtypes: dbtypes,
            numlocals: numlocals,
            datelocals: datelocals
        });
    });
});

/* POST Modify. */
router.post('/do', function(req, res, next) {
    req.sanitizeBody('name').trim();
    req.sanitizeBody('dbtype').trim().toLowerCase();
    req.sanitizeBody('numlocal').trim();
    req.sanitizeBody('datelocal').trim();
    req.sanitizeBody('dbdatabase').trim();
    req.sanitizeBody('dbhost').trim();
    req.sanitizeBody('dbport').trim();
    req.sanitizeBody('dbusername').trim();
    req.sanitizeBody('dbpassword').trim();
    
    req.checkBody('pathName', 'Invalid path name').notEmpty().withMessage('Path name is required').isAlphanumeric().isLength(2, 100);
    req.checkBody('numlocal', 'Invalid number locale').notEmpty().withMessage('Number locale is required').isAscii().isLength(2, 100);
    req.checkBody('datelocal', 'Invalid date locale').notEmpty().withMessage('Date locale is required').isAscii().isLength(2, 100);
    req.checkBody('name', 'Invalid name').notEmpty().withMessage('Name is required').isLength(2, 100);
    req.checkBody('port', 'Invalid port (80-65535)').notEmpty().withMessage('Port is required').isInt({ min: 80, max: 65535 });
    req.checkBody('dbtype', 'Invalid database type').notEmpty().withMessage('Database type is required').isAlphanumeric().isLength(5, 10);
    
    if (req.body.dbport) {
        req.checkBody('dbport', 'Invalid database port (1-65535)').isInt({ min: 1, max: 65535 });
    }
    var errors = req.validationErrors();
    if (errors) {
        res.render('error', { error: {status: 'Validation error', validation: errors} }); return;
    }
    
    //Modify
    var e1apps = require('../lib/e1apps');
    e1apps.modify(
        req.body.pathName,
        req.body.name,
        parseInt(req.body.port),
        req.body.numlocal,
        req.body.datelocal,
        req.body.dbtype,
        req.body.dbdatabase,
        req.body.dbhost,
        req.body.dbport ? parseInt(req.body.dbport) : null,
        req.body.dbusername,
        req.body.dbpassword
    );
    
    res.redirect('/list');
});

module.exports = router;
