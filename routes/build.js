var express = require('express');
var router = express.Router();
var debug = require('debug')('Every1Builder:server');

/* GET Build. */
router.get('/:pathName', function(req, res, next) {
    req.checkParams('pathName', 'Invalid path name').notEmpty().withMessage('Path name is required').isAlphanumeric().isLength(2, 100);
    var errors = req.validationErrors();
    if (errors) {
        res.render('error', { error: {status: 'Validation error', validation: errors} }); return;
    }
    
    //Build
    var e1apps = require('../lib/e1apps');
    e1apps.build(req.params.pathName, function(err, app, steps) {
        if (err) {
            res.render('error', { error: {status: 'Error', stack: [err], steps: steps} }); return;
        }
        debug('Build app', app);
        res.render('build', {
            app: app, 
            steps: steps,
            runPath: create_run_path(app)
        });
    });
});


/* GET Run app. */
router.get('/run/:pathName', function(req, res, next) {
    req.checkParams('pathName', 'Invalid path name').notEmpty().withMessage('Path name is required').isAlphanumeric().isLength(2, 100);
    var errors = req.validationErrors();
    if (errors) {
        res.status(500).json({ error: {status: 'Validation error', validation: errors} }); return;
    }
    
    var e1main = require('../lib/e1main');
    e1main.openInfo_GetBase(req.params.pathName, function(err, app) {
        if (err) {
            res.status(500).json({ error: {status: 'Error', error: err} }); return;
        }
        
        try {
            var open = require("open");
            debug('Opening '+app.folderPath);
            open(create_run_path(app), function (err) {
                if (err) {
                    res.status(500).json({ error: {status: 'Error running app', error: err} });
                } else {
                    res.send('done');
                }
            });
        } catch (err) {
            res.status(500).json({ error: {status: 'Error prepare running app', error: err} });
        }
    });
});

/**
 * Create run  path
 * 
 * @param   {object}        app         App info
 * @returns {String}
 */
function create_run_path(app) {
    return app.folderPath+'/build/start';
//    return 'cd '+app.folderPath+'/build; npm start';
}

module.exports = router;
